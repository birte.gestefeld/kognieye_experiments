function check_key(ansKey,el,dummymode,edfFile,b)
% function that checks if esc or c have been pressed during a trial to stop the experiment or to calibrate again
% INPUT:
%           ansKey = int with keyCode
%           dummymode = logical variable
% Birte Gestefeld, 2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de

if ansKey == 10 % if ESC, we stop the experiment
    ShowCursor;
    if ~dummymode
        % finishing
        
        Eyelink('StopRecording');
        WaitSecs(0.001);
        Eyelink('Message', 'TRIAL_RESULT 0');
        
        Eyelink('CloseFile');
        sca
        
            % download data file
            try
                fprintf('Receiving data file ''%s''\n', edfFile);
                status=Eyelink('ReceiveFile');
                if status > 0
                    fprintf('ReceiveFile status %d\n', status);
                end
                if 2==exist(edfFile, 'file')
                    fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd);
                end
            catch rdf
                fprintf('Problem receiving data file ''%s''\n', edfFile );
                rdf;
            end
        
    end
    
    fclose('all');

    return
elseif ansKey == 55 % C (calibration) in case we need to calibrate again
            EyelinkDoTrackerSetup(el);
            EyelinkDoDriftCorrection(el);
    

elseif ansKey == 34 % C (calibration) in case we need to calibrate again
            Eyelink('Message', 'PAUSE');
            EyelinkDoDriftCorrection(el);
            
    
end
end


