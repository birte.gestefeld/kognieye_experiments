
function pix = deg2pix(deg, xres, width, view_dist)
% function to convert degrees of visual angle in pixels
%
% pix = deg2pix(deg, xres, width, view_dist)
%
% pix:          the quantity in pixels
% xres:         horizontal resolution of the screen
% width:        physical width of the screen
% view_dist:    viewing distance
% Birte Gestefeld  2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de

pix=tan(deg2rad(deg/2)) *2*view_dist * xres/width; 

end
