function el = prepEyeLink(screen, edfFile,dummymode)
%function prepEyeLink
%   Prepares EyeLink for experiment.
%       INPUT:
%           scr = struct with screen info
%           vis = struct with visual info
%       OUTPUT:
%           el = struct with Eyelink info
%
% Christian Poth, 2019, c.poth@uni-bielefeld.de
% Birte Gestefeld edited for KogniEye, 2021, birte.gestefeld@uni-bielefeld.de


[~,vs] = Eyelink('GetTrackerVersion');
fprintf('Running experiment on a ''%s'' tracker.\n', vs ); % shows the eye tracker version, helps to check if we are in dummy mode

% Load eyelink defaults and mute the beep sounds under Ubuntu 
el=EyelinkInitDefaults(screen.window);
el.targetbeep = 0; % currently no sound during calibration
el.feedbackbeep = 0;
EyelinkUpdateDefaults(el);

EyelinkInit(dummymode, 1)

% open the edf file to sace the data
Eyelink('Openfile', edfFile);
% Set up tracker configuration.
Eyelink('command', 'add_file_preamble_text ''Christian Poth, c.poth@uni-bielefeld.de''');
Eyelink('command', 'add_file_preamble_text ''Birte Gestefeld, birte.gestefeld@uni-bielefeld.de''');

% Map gaze to px.
Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, screen.xScreen-1, screen.yScreen-1);
Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, screen.xScreen-1, screen.yScreen-1);
Eyelink('message', 'FRAMERATE %ld ', screen.hz);

% Calibration type.
Eyelink('command', 'calibration_type = HV9');
Eyelink('command', 'generate_default_targets = YES');

% Set parser (conservative saccade thresholds).
Eyelink('command', 'saccade_velocity_threshold = 35');
Eyelink('command', 'saccade_acceleration_threshold = 9500');

% EDF file contents.
Eyelink('command', 'file_event_filter  = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE');
Eyelink('command', 'file_sample_data   = LEFT,RIGHT,GAZE,GAZERES,AREA');
Eyelink('command', 'link_event_filter  = LEFT,RIGHT,FIXATION,SACCADE,BLINK');
Eyelink('command', 'link_sample_data   = LEFT,RIGHT,GAZE,AREA');

% Camera setup.
Eyelink('command', 'sample_rate = %d', 1000); % automatically 

