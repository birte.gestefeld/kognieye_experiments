function run_memoryguided(screen,stim,el,dummymode,ptc,t,b,edfFile,trial)
if dummymode
    ShowCursor
    Screen('ShowCursorHelper', screen.window)
end

% we first select the position of the target or distractor in
% each trial at random ( 8 or 2 positions?
% select new position if trial is not repeated
%edited 15.12.21
%stim.pos=randSample([5 1],1);
[tXpos, tYpos, tXpos_deg, tYpos_deg] = dotPosition(screen,stim);


% then we need to determine the foreperiod (same as in the pro- and
% antisaccade tasks)
fp = (stim.endFP-stim.startFP).* rand + stim.startFP;
%% we only present the FIXATION CROSS during the foreperiod
% grey background
Screen('FillRect', screen.window, screen.grey);
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblFixOnset = Screen('Flip', screen.window);
fd =0;
fixError =0;
if ~dummymode
    Eyelink('Message', 'FIXATION_DISPLAY');
end

%then we wait
while fd <= vblFixOnset + fp - 0.5 * screen.ifi
    fd = GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el, dummymode, edfFile,b);
    end
end


%% then we flash the target
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
Screen('DrawDots', screen.window, [tXpos tYpos], stim.dotSize, screen.black, [], 2);
vblFlashOnset = Screen('Flip', screen.window);
if ~dummymode
    Eyelink('Message', 'TARGET_FLASH');
end

%% then two seconds only the fixation cross: memory period.
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblMemOnset=Screen('Flip', screen.window, vblFlashOnset + stim.flash - 0.5 * screen.ifi);
md =0;
if ~dummymode
    Eyelink('Message', 'MEMORY_PERIOD');
end

while md <= vblMemOnset + stim.memPeriod - 0.5 * screen.ifi
    md = GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el, dummymode, edfFile,b);
    end
    if ~dummymode && Eyelink('NewFloatSampleAvailable') > 0 %
        evt = Eyelink('NewestFloatSample');
        eyeX = evt.gx(el.eye_used+1); % x und y Koordinate
        eyeY = evt.gy(el.eye_used+1);
        if eyeX~=el.MISSING_DATA && eyeY~=el.MISSING_DATA && evt.pa(el.eye_used+1)>0 % wenn wir keinen blink oder andere ungueltige daten haben
            distance = sqrt( (stim.crossXpos-eyeX)^2 + (stim.crossYpos-eyeY)^2 );% distanz zum target
            if distance > stim.fixError
                fixError = fixError +1;
            end
        end
        
    else % this dummymode option can be deleted once we test for real
        [mouseX, mouseY, ~] = GetMouse(screen.window);
        distance = sqrt( (stim.crossXpos-mouseX)^2 + (stim.crossYpos-mouseY)^2 );% distanz zum target
        if distance > stim.fixError
            fixError = fixError +1;
        end
    end
end

%% then the blank screen 2 seconds: saccade period
% blank screen presentation time
Screen('FillRect', screen.window, screen.grey);
vblBlankOnset = Screen('Flip', screen.window);
tp1=0;
targetFix =0;
if ~dummymode
    Eyelink('Message', 'STIMULUS_DISPLAY');
end

% check if the the target gets fixated
while tp1 <= vblBlankOnset + stim.trialLength - 0.5 * screen.ifi
    tp1 = GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el, dummymode, edfFile,b);
    end
    if ~dummymode && Eyelink( 'NewFloatSampleAvailable') > 0 % get eye position
        evt = Eyelink('NewestFloatSample');
        eyeX = evt.gx(el.eye_used+1); % x and y coordinate of the used eye
        eyeY = evt.gy(el.eye_used+1);
        if eyeX~=el.MISSING_DATA && eyeY~=el.MISSING_DATA && evt.pa(el.eye_used+1)>0 % wenn wir keinen blink oder andere ungueltige daten haben
            distance = sqrt( (tXpos-eyeX)^2 + (tYpos-eyeY)^2 ); % distanz zum target
            if distance < stim.memDist
                targetFix = targetFix + 1;
                if targetFix>20
                    Eyelink('Message', 'TARGET_FIXATION');
                    WaitSecs(1);
                    break;
                end
            end
        end
    else % im dummymode machen wir das gleiche mit der Maus
        [mouse_x, mouse_y, ~] = GetMouse(screen.window);
        distance = sqrt( (tXpos-mouse_x)^2 + (tYpos-mouse_y)^2 );
        if distance < stim.memDist
            targetFix = targetFix + 1;
            if targetFix>20
                disp('TARGET_FIXATION')
                WaitSecs(1);
                break;
            end
        end
    end
end
rt1 = tp1 - vblBlankOnset;
    
    if ~dummymode & ~trial
        WaitSecs(0.1);
        Eyelink('StopRecording');
        % Messages for trial variables.
        Eyelink('Message', '!V TRIAL_VAR participant %s', num2str(ptc.code));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR age %s', num2str(ptc.age));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR gender %s', num2str(ptc.gender));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR handedness %s', num2str(ptc.hand));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR vision %s', num2str(ptc.visionOk));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR trial %d', t);
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR block %d', b);
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR target_xpos %s',  num2str(tXpos));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_ypos %s',  num2str(tYpos));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_xpos_deg %s',  num2str(tXpos_deg));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_ypos_deg %s',  num2str(tYpos_deg));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR reaction_time1 %s', num2str(rt1)); % time the target was presented before the participant response
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR fixation_error %d', fixError); % time the target was presented before the participant response
        WaitSecs(0.001)
          % Message of trial end.
        Eyelink('Message', 'TRIAL_RESULT 0');
    end
    
end
