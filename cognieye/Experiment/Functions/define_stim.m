function stim = define_stim(screen)
% function define_stim
%   defines parameters of the different stimuli used
%       INPUT:
%           screen = struct with Screen info needed to run the experiment
%       OUTPUT:
%            stim = struct with parameters to draw the different stimuli
%
% Birte Gestefeld  2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de

% define the instruction texts
stim = struct();
instructionText = cell(3,1);
instructionText{1} = ['Dieser Block besteht aus 60 Durchgängen.\n' ...
'Am Anfang jedes Durchgangs erscheint ein Punkt in der Mitte des Bildschirms. Schauen Sie diesen Punkt an. \n' ...
'Nach kurzer Zeit verschwindet der Punkt und taucht entweder auf der linken oder rechten Seite des Bildschirms wieder auf. \n' ...
'Schauen Sie den Punkt so schnell wie möglich wieder an. Schauen Sie auch so genau wie möglich auf den Punkt. \n' ...
'Sehen Sie den Punkt, auch wenn er auf der linken oder rechten Seite erscheint, so lange an bis ein neuer Durchgang beginnt.']; % first block pro-saccade task
instructionText{2} = ['Dieser Block besteht aus 40 Durchgängen.\n' ...
'Am Anfang jedes Durchgangs erscheint ein Punkt in der Mitte des Bildschirms. Schauen Sie diesen Punkt an. \n' ...
'Nach kurzer Zeit verschwindet der Punkt und taucht entweder auf der linken oder rechten Seite des Bildschirms wieder auf. \n' ...
 'Sehen Sie diesen Punkt nicht an, sondern schauen Sie genau auf die gegenüberliegede Seite. \n' ...
 'Sehen Sie so schnell und genau wie möglich an die dem Punkt genau gegenüberliegede Stelle (vom Mittelpunkt aus gespiegelt). \n' ...
 'Sehen Sie an diese Stelle, so lange  bis ein neuer Durchgang beginnt.']; 
instructionText{3} = ['Dieser Block besteht aus 40 Durchgängen.\n' ...
'Am Anfang jedes Durchgangs erscheint ein Punkt in der Mitte des Bildschirms. Schauen Sie diesen Punkt an. \n' ...
    'Nach kurzer Zeit sehen Sie sehr kurz einen weiteren Punkt, links oder rechts aufflackern. \n' ...
    'Sehen SIe diesen aufflackernden Punkt nicht an, sondern schauen Sie weiterhin den Punkt in der Mitte an. \n' ...
    'Nach kurzer Zeit verschwindet der schwarze Punkt in der Mitte.\n' ...
    'Sobald der schwarze Punkt verschwindet, schauen Sie an die Stelle an der der andere Punkt kurz aufgetaucht war. \n' ...
    'Sehen Sie an diese Stelle, so lange  bis ein neuer Durchgang beginnt.']
    


stim.instructionText = instructionText;

% stimulus parameters for all stimuli
stim.sacLen = 8; % in degrees of visual angle
stim.dotSize = deg2pix(0.5, screen.xScreen, screen.width, screen.viewDist); % in degrees of visual angle
stim.startFP = 1; % start foreperiod for pro and ati saccade task 
stim.endFP = 3.5; % end foreperiod
stim.trialLength = 2; % actually too long according to 'an internationally standardised anti saccade protocoll'. should be 1 second
stim.targetDist = deg2pix(3, screen.xScreen, screen.width, screen.viewDist);% how close eye position needs to be to the target area to be counted as finding the target
stim.fixError = deg2pix(3, screen.xScreen, screen.width, screen.viewDist); % how far eye position needs to be from fixation dot to be counted as fixation error
stim.crossXpos = screen.xCenter;
stim.crossYpos = screen.yCenter;    

% stimulus parameter for the memory guided saccades 
stim.flash = 0.15; % how long the stimulus gets flashed for the memeory guided saccade
stim.memSac=2; % memory guided saccade task time they have to make the saccade
stim.memDist = deg2pix(3, screen.xScreen, screen.width, screen.viewDist); % distance that the eye position needs to have to 
stim.memPeriod = 2; % period the position of the flash needs to be 
stim.blankPeriod=2;


% stimuli for the anti saccade task
stim.corDist = deg2pix(2, screen.xScreen, screen.width, screen.viewDist); % distance to the target to be counted as corrective saccade
stim.fixcrossDist= deg2pix(3.5, screen.xScreen, screen.width, screen.viewDist); % how far the eyes need to move away from the fixation in degrees to be counted as (anti)saccade




end
