function design = make_design()
  %   randomly selects the order of the blocks 

%       OUTPUT:
%           design = struct with order of the blocks and the number of trials per block
%          
% Birte Gestefeld, 2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de
% 60 Prosaccades;40 antisaccades;40 antisaccades;40 antisaccades;60 prosaccades :

design.block = [1 2 2 2 1 3 3];
design.trial = [60 40 40 40 60 40 40];
% edited 15.12.21
vpro= [repmat(1,1,30),repmat(5,1,30)]; 
design.pos_pro=vpro(randperm(length(vpro)));
vanti=[repmat(1,1,20),repmat(5,1,20)];
design.pos_anti_mem = vanti(randperm(length(vanti))); 

end