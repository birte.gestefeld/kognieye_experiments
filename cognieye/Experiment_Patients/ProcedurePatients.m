% main script adapted to be run with patients
% Birte Gestefeld, 2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de

clear all;
close all;
fclose all;
clc;
cd '/home/experimenter/Dokumente/cognieye/Experiment_Patients'
%cd 'C:\Users\Display\Documents\KogniEye\cognieye\Experiment_Patients' % path under Windows

addpath ('Functions_Patients');
dummymode = 0 ;

% Get participant info.
ptc = getPtc('CogniEye');
% we randomly select the order in which we present the blocks
design = make_design_patients();

%% Initialize screen
Screen('Preference', 'SkipSyncTests', 0); % nicht benutzen, wenn wir wirklich testen unter UBuntu und Octave
screens = Screen('Screens');
screen=define_screen(screens);

%% Objects for all trials
stim = define_stim_patients(screen);

for b=1:length(design.block)
     %% create eye tracking data file
     edfFile = [ptc.code 'B' num2str(b),'S' num2str(ptc.session)];
     edfFilePractice = [ptc.code 'B' num2str(b),'P']; 
     %% Eye tracker Setup
     
    
    % start with 10 practice trials before first prosaccade block
    if b ==1
      if ~dummymode
        el = prepEyeLink(screen, edfFilePractice,dummymode);
        Screen('HideCursorHelper', screen.window)
        % calibration
        EyelinkDoTrackerSetup(el);

    else
        el ='dummy';
        %ShowCursor;
        Screen('ShowCursorHelper', screen.window)
    end
        for t = 1:10
            if ~dummymode
              
                Eyelink('Message', 'PRACTICE TRIAL PROSACCADE');
                Eyelink('StartRecording');
                WaitSecs(0.1);
                % check which eye we record
                el.eye_used = Eyelink('EyeAvailable');
                if el.eye_used == 2 % if binocular, we select the right eye curretly.point of discussion
                    el.eye_used =1;
                end
                
                % eyelink needs to sync
                Eyelink('Message', 'SYNCTIME');
            end
            
            if t==1
                Screen('FillRect',screen.window, screen.black);
                Screen('TextSize', screen.window, 30);
                DrawFormattedText(screen.window, stim.instructionText{1}, 100,300, screen.white);
                Screen('Flip',screen.window);
                WaitSecs(0.2); % trial starts when button was pressed
                 [secs,keyCode,~]=KbWait();
                kC = find(keyCode)
                if kC == 40
                  b=b+1;
                  break
                end
            end
            
            run_prosacccade_practice(screen,stim,el,dummymode, ptc,t,b); %run practice trials
          end
          Eyelink('CloseFile');
            try
            fprintf('Receiving data file ''%s''\n', edfFilePractice);
            status=Eyelink('ReceiveFile');
            if status > 0
                fprintf('ReceiveFile status %d\n', status);
            end
            if 2==exist(edfFile, 'file')
                fprintf('Data file ''%s'' can be found in ''%s''\n', edfFilePractice, pwd);
            end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFilePractice);
            rdf;
        end
     
        
    elseif b==3 %erster antisakkadenblock
        el = prepEyeLink(screen, edfFilePractice,dummymode);
                Screen('HideCursorHelper', screen.window)
                % calibration
               # EyelinkDoTrackerSetup(el);
        for t = 1:10
            if ~dummymode
               
                Eyelink('Message', 'PRACTICE TRIAL ANTISACCADE');
                Eyelink('StartRecording');
                WaitSecs(0.1);
                % check which eye we record
                el.eye_used = Eyelink('EyeAvailable');
                if el.eye_used == 2 % if binocular, we select the right eye curretly.point of discussion
                    el.eye_used =1;
                end
                
                % eyelink needs to sync
                Eyelink('Message', 'SYNCTIME');
            end
            if t==1
                Screen('FillRect',screen.window, screen.black);
                Screen('TextSize', screen.window, 30);
                DrawFormattedText(screen.window, stim.instructionText{2}, 100,300, screen.white);
                Screen('Flip',screen.window);
                WaitSecs(0.2); % trial starts when button was pressed
                [secs,keyCode,~]=KbWait();
                kC = find(keyCode)
                if kC == 40
                  b=b+1;
                  break
                end
            end
            run_prosacccade_practice(screen,stim,el,dummymode, ptc,t,b); %run practice trials
        end
        Eyelink('CloseFile');
        try
            fprintf('Receiving data file ''%s''\n', edfFilePractice);
            status=Eyelink('ReceiveFile');
            if status > 0
                fprintf('ReceiveFile status %d\n', status);
            end
            if 2==exist(edfFile, 'file')
                fprintf('Data file ''%s'' can be found in ''%s''\n', edfFilePractice, pwd);
            end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFilePractice );
            rdf;
        end
    elseif b==2 %erster memory guided
        el = prepEyeLink(screen, edfFile,dummymode);
                Screen('HideCursorHelper', screen.window)
                % calibration
                #EyelinkDoTrackerSetup(el);
        for t = 1:10
            if ~dummymode
              
                Eyelink('Message', 'PRACTICE TRIAL MEMORY GUIDED SACCADE');
                Eyelink('StartRecording');
                WaitSecs(0.1);
                % check which eye we record
                el.eye_used = Eyelink('EyeAvailable');
                if el.eye_used == 2 % if binocular, we select the right eye curretly.point of discussion
                    el.eye_used =1;
                end
                
                % eyelink needs to sync
                Eyelink('Message', 'SYNCTIME');
            end
            if t==1
                Screen('FillRect',screen.window, screen.black);
                Screen('TextSize', screen.window, 30);
                DrawFormattedText(screen.window, stim.instructionText{3}, 100,300, screen.white);
                Screen('Flip',screen.window);
                WaitSecs(0.2); % trial starts when button was pressed
                [secs,keyCode,~]=KbWait();
                kC = find(keyCode)
                if kC == 40
                  b=b+1;
                  break
                end
            end
            run_memoryguided_practice(screen,stim,el,dummymode,ptc,t,b); % run 10 practice trials slowly
        end
        Eyelink('CloseFile');
        try
            fprintf('Receiving data file ''%s''\n', edfFilePractice);
            status=Eyelink('ReceiveFile');
            if status > 0
                fprintf('ReceiveFile status %d\n', status);
            end
            if 2==exist(edfFile, 'file')
                fprintf('Data file ''%s'' can be found in ''%s''\n', edfFilePractice, pwd);
            end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFilePractice );
            rdf;
        end
    end
   
   if ~dummymode 
        el = prepEyeLink(screen, edfFile,dummymode);
        Screen('HideCursorHelper', screen.window)
        % calibration
        EyelinkDoTrackerSetup(el);
       
    else
        el ='dummy';
        %ShowCursor;
        Screen('ShowCursorHelper', screen.window)
    end
    
    for t=1:design.trial(b)
        %% before the first trial: draw trial instructions
        if t==1
            EyelinkDoDriftCorrection(el)
            Screen('FillRect',screen.window, screen.black);
            Screen('TextSize', screen.window, 30);
            DrawFormattedText(screen.window, stim.instructionText{design.block(b)}, 100,300, screen.white);
            Screen('Flip',screen.window);
            WaitSecs(0.2); % trial starts when button was pressed
             [secs,keyCode,~]=KbWait();
                kC = find(keyCode)
                if kC == 40
                  b=b+1;
                  break
                end
            
        end
        
        
        %% trial start
        % make sure we keep track of the trial type
        if ~dummymode %
            switch design.block(b)
                case 1
                    blockID = 'prosaccade';
                case 2
                    blockID = 'antisaccade';
                case 3
                    blockID = 'memory_guided';
                    
            end

            Eyelink('Message', 'BLOCKID %s', blockID);
            Eyelink('Message', 'TRIALID %d', t); % absolute trial, unique within an edf.
            % start the eye movement recording
            Eyelink('StartRecording');
            WaitSecs(0.1);
            
            % check which eye we record
            el.eye_used = Eyelink('EyeAvailable');
            if el.eye_used == 2 % if binocular, we select the right eye curretly.point of discussion
                el.eye_used =1;
            end
            
            % eyelink needs to sync
            Eyelink('Message', 'SYNCTIME');
            
        end
        
        
        %% depending on which block we are in, we run a diffferent routine here
        switch design.block(b)
            case 1 % in case of the pro-saccade task
                stim.pos= design.pos(t)
                run_prosacccade(screen,stim, el, dummymode, ptc,t,b,edfFile,0); % classic pro-saccade task
            case 2
                stim.pos= design.pos(t)
                run_antisacccade(screen,stim,el, dummymode, ptc,t,b,edfFile,0);
            case 3
                stim.pos= design.pos(t)
                run_memoryguided(screen,stim,el,dummymode, ptc,t,b,edfFile,0);
        end
        
    end
    % end the experiment and close the edf file
    if ~dummymode
        Eyelink('CloseFile');
        try
            fprintf('Receiving data file ''%s''\n', edfFile);
            status=Eyelink('ReceiveFile');
            if status > 0
                fprintf('ReceiveFile status %d\n', status);
            end
            if 2==exist(edfFile, 'file')
                fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd);
            end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFile );
            rdf;
        end
    end
    
end
sca
