function run_prosacccade_practice(screen,stim, el,dummymode,ptc,t,b)
if dummymode
    ShowCursor
    Screen('ShowCursorHelper', screen.window)
end
% we first select the position of the target or distractor in
% each trial at random

stim.pos=randSample([5 1],1); %classic prosaccade task either to the left or to the right
[tXpos, tYpos, tXpos_deg, tYpos_deg] = dotPosition(screen,stim);


% foreperiodh is randomly selected
fp = (stim.endFP-stim.startFP).* rand + stim.startFP;

% beginning of the trial: dot is presented in the center of the grey screen
Screen('FillRect', screen.window, screen.grey);
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblFixOnset=Screen('Flip', screen.window);
Screen('GetFlipInterval', screen.window)
fd =0;

 KbStrokeWait();
       
    % present the black dot either to the left or to the right
    Screen('DrawDots', screen.window, [tXpos tYpos], stim.dotSize, screen.black, [], 2); % dot is now presented to the left or to the right
    vblStimOnset=Screen('Flip', screen.window); % screen gets flipped when foreperiod is over
    if ~dummymode
        Eyelink('Message', 'STIMULUS_DISPLAY');
    end
    KbStrokeWait();
       
end