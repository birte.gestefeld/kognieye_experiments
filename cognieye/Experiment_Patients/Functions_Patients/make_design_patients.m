function design = make_design_patients()
  %   randomly selects the order of the blocks 

%       OUTPUT:
%           design = struct with order of the blocks and the number of trials per block
%          
% Birte Gestefeld, 2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de
% 60 Prosaccades;40 antisaccades;40 antisaccades;40 antisaccades;60 prosaccades :

% design.block = [1 2 2 2 1 3 3];
% design.trial = [60 40 40 40 60 40 40];

design.block = [1 3 2 2 3 1];
design.trial = [40 40 40 40 40 40];
vtrial= [repmat(1,1,20),repmat(5,1,20)]; 
design.pos=vtrial(randperm(length(vtrial)));
end