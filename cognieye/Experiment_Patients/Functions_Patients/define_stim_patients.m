function stim = define_stim_patients(screen)
% function define_stim
%   defines parameters of the different stimuli used
%       INPUT:
%           screen = struct with Screen info needed to run the experiment
%       OUTPUT:
%            stim = struct with parameters to draw the different stimuli
%
% Birte Gestefeld  2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de

% define the instruction texts
stim = struct();
instructionText = cell(3,1);
instructionText{1} = ['Schauen Sie immer auf den schwarzen Punkt. \n' ...
'Der Punkt wird im Laufe des Experiments nach links oder rechts springen. \n' ...
'Folgen Sie ihm so schnell wie moeglich mit Ihrem Blick  \n' ...
'und schauen Sie ihn danach weiterhin an.']; % first block pro-saccade task
instructionText{2} = ['Schauen Sie am Anfang des Experimentes auf den schwarzen Punkt im Zentrum des Bildschirms. \n' ...
    'Sobald der Punkt an eine anderen Stelle springt (nach links oder rechts), schauen Sie so schnell wie moeglich, \n' ...
    'in die gegenueberliegende Richtung. \n' ... 
    'Probieren Sie ihren Blick moeglichst genau auf die gegnueberliegende Stelle des Punktes,  \n' ...
    'in gleichem Abstand zur Mitte des Bildschirms, zu richten, \n' ...
    'bis erneut ein schwarzer Punkt in der Mitte des Bildschirms zu sehen ist.'];
instructionText{3} = ['Schauen Sie am Anfang des Experimentes auf den schwarzen Punkt in der Mitte. \n' ...
    'Nach kurzer Zeit sehen Sie sehr kurz einen weiteren Punkt, links oder rechts der Mitte, erscheinen. \n' ...
    'Sehen Sie weiterhin den schwarzen Punkt in der Mitte an. \n' ...
    'Sobald der schwarze Punkt verschwindet, schauen Sie an die Stelle an der der andere Punkt kurz aufgetaucht war. \n' ...
    'Sehen Sie so lange an diese Stelle bis erneut ein schawrzer Punkt in der Mitte erscheint. \n' ...
    'Manchmal erscheint auch noch einmal ein Punkt links oder rechts. \n' ...
    'Schauen Sie auch diesen Punkt an, soblad Sie ihn sehen und fixieren Sie ihn bis er verschwindet.'];


stim.instructionText = instructionText;

% stimulus parameters for all stimuli
stim.sacLen = 8; % in degrees of visual angle
stim.dotSize = deg2pix(0.5, screen.xScreen, screen.width, screen.viewDist); % in degrees of visual angle
stim.startFP = 1; % start foreperiod for pro and ati saccade task 
stim.endFP = 3.5; % end foreperiod
stim.trialLength = 2; % actually too long according to 'an internationally standardised anti saccade protocoll'. should be 1 second
stim.targetDist = deg2pix(3, screen.xScreen, screen.width, screen.viewDist);% how close eye position needs to be to the target area to be counted as finding the target
stim.fixError = deg2pix(3, screen.xScreen, screen.width, screen.viewDist); % how far eye position needs to be from fixation dot to be counted as fixation error
stim.crossXpos = screen.xCenter;
stim.crossYpos = screen.yCenter;    

% stimulus parameter for the memory guided saccades 
stim.flash = 0.15; % how long the stimulus gets flashed for the memeory guided saccade
stim.memSac=2; % memory guided saccade task time they have to make the saccade
stim.memDist = deg2pix(3, screen.xScreen, screen.width, screen.viewDist); % distance that the eye position needs to have to 
stim.memPeriod = 2; % period the position of the flash needs to be 
stim.blankPeriod=2;


% stimuli for the anti saccade task
stim.corDist = deg2pix(2, screen.xScreen, screen.width, screen.viewDist); % distance to the target to be counted as corrective saccade
stim.fixcrossDist= deg2pix(3.5, screen.xScreen, screen.width, screen.viewDist); % how far the eyes need to move away from the fixation in degrees to be counted as (anti)saccade




end
