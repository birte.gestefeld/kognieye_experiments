function run_memoryguided_practice(screen,stim,el,dummymode,ptc,t,b,edfFile,trial)
if dummymode
    ShowCursor
    Screen('ShowCursorHelper', screen.window)
end

% we first select the position of the target or distractor in
% each trial at random ( 8 or 2 positions?
% select new position if trial is not repeated
stim.pos=randSample([5 1],1);
[tXpos, tYpos, tXpos_deg, tYpos_deg] = dotPosition(screen,stim);
% then we need to determine the foreperiod (same as in the pro- and
% antisaccade tasks)
fp = (stim.endFP-stim.startFP).* rand + stim.startFP;
%% we only present the FIXATION CROSS during the foreperiod
% grey background
Screen('FillRect', screen.window, screen.grey);
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblFixOnset = Screen('Flip', screen.window);

KbStrokeWait();
%% then we flash the target
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
Screen('DrawDots', screen.window, [tXpos tYpos], stim.dotSize, screen.black, [], 2);
vblFlashOnset = Screen('Flip', screen.window);

%% then two seconds only the fixation cross: memory period.
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblMemOnset=Screen('Flip', screen.window, vblFlashOnset + stim.flash - 0.5 * screen.ifi);
KbStrokeWait();

%% then the blank screen 2 seconds: saccade period
% blank screen presentation time
Screen('FillRect', screen.window, screen.grey);
vblBlankOnset = Screen('Flip', screen.window);
KbStrokeWait();

   
end
