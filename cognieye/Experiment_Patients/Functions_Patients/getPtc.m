function ptc = getPtc(expName)
%function ptc = getPtc(expName)
% Collects participant and experiment information.
%   INPUT:
%       expName = name of experiment
%   OUTPUT:
%       ptc = struct with information on participant and experiment
%
% Christian Poth, 2019, c.poth@uni-bielefeld.de

while 1
    
    ptc.expName = expName;
    ptc.ptcNo = str2double(input('Versuchspersonennummer:', 's'));
    ptc.code = input('Versuchspersonen-Code:', 's');
    ptc.session = str2double(input('Sitzungsnummer:', 's'));
    ptc.age = input('Alter der Versuchsperson (in Jahren):', 's');
    ptc.gender = input('Geschlecht der Versuchsperson\n(w = weiblich, m = maennlich, a = andere/keine Angabe):', 's');
    ptc.hand = input('Haendigkeit der Versuchsperson\n(l = linkshaendig, r = rechtshaendig, b = beidhaendig):', 's');
    ptc.visionOk = input('Ist das Sehen unbeeintraechtigt oder korrigiert? (u = unbeeintraechtigt, k = Kontaktlinsen, b = Brille):', 's');
        
    correct = input('Sind alle Angaben korrekt? (j = ja, n = nein): ', 's');
    if strcmp(correct, 'j')
        break;
    end
    
end
