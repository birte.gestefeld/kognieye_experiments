
function [screen] = define_screen(screens)
% function define_screen
%   defines screen for experiment.
%       INPUT:
%           screens = output of Psychtoolbox function Screen('Screens')
%           screen = struct with Screen info needed to run the experiment
%
% Birte Gestefeld  2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de


screen.screenNumber = max(screens);
% defining grey,white and black;

screen.rect= [0 0 800 500];
screen.grey=GrayIndex(screen.screenNumber);
screen.black = BlackIndex(screen.screenNumber);
screen.white = WhiteIndex(screen.screenNumber);
%PsychDebugWindowConfiguration();
[screen.window, screen.windowRect] = Screen('OpenWindow', screen.screenNumber, screen.grey);
Screen('Flip', screen.window);
screen.ifi = Screen('GetFlipInterval', screen.window);
screen.hz = Screen('FrameRate', screen.window);
% interframe intervall
screen.topPriorityLevel = MaxPriority(screen.window);  % set top priority
Screen('BlendFunction', screen.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');


% meisures of screen to calculate from degrees to pixels
% meisured height and width of the screen
screen.width =380;
screen.height =215;
% viewing distance
screen.viewDist= 600;

[screen.xCenter, screen.yCenter] = RectCenter(screen.windowRect);     % (0,0) des Schirms wichtig fuer die Position der Punkte
[screen.xScreen, screen.yScreen]=Screen('WindowSize', screen.window);

HideCursor
end
