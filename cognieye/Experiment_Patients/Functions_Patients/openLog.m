function expLog = openLog(saveFolder, fileName, varNames)
%function expLog = openLog(fileName, nLogVar, varNames)
%   OUTPUT: Log-file.
%   INPUT: 
%       saveFolder = folder where data shall be saved
%       fileName = desired file name
%       nLogVar = number of variables to log
%       varNames = cell with strings of variable names
%
% Christian Poth, c.poth@uni-bielefeld.de, 2019

if ~exist(fullfile(saveFolder, fileName))
    expLog = fopen(fullfile(saveFolder, fileName), 'w');
else
    clear all;
    clear mex;
    clear functions;
    close all;
    sca;
    error(['Do not overwrite data file!'])
end

if expLog == -1
    error(['Could not open ' fileName ' for writing'])
end

fprintf(expLog, [repmat('%s\t', 1, size(varNames, 2) - 1) '%s\n'], varNames{:});

