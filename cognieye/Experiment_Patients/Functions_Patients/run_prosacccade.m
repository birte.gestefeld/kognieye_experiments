function run_prosacccade(screen,stim, el,dummymode,ptc,t,b,edfFile,practice)
if dummymode
    ShowCursor
    Screen('ShowCursorHelper', screen.window)
end
% we first select the position of the target or distractor in
% each trial at random
% bug fix 21.12
%classic prosaccade task either to the left or to the right
[tXpos, tYpos, tXpos_deg, tYpos_deg] = dotPosition(screen,stim);


% foreperiodh is randomly selected
fp = (stim.endFP-stim.startFP).* rand + stim.startFP;

% beginning of the trial: dot is presented in the center of the grey screen
Screen('FillRect', screen.window, screen.grey);
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2);
vblFixOnset=Screen('Flip', screen.window);
fd =0;

if ~dummymode
    Eyelink('Message', 'FIXATION_DISPLAY');
end

% check for a key press during foreperiod
while fd <= vblFixOnset + fp - 0.5 * screen.ifi
    fd = GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el,dummymode,edfFile,b);
    end
end
    
    % present the black dot either to the left or to the right
    Screen('DrawDots', screen.window, [tXpos tYpos], stim.dotSize, screen.black, [], 2); % dot is now presented to the left or to the right
    vblStimOnset=Screen('Flip', screen.window); % screen gets flipped when foreperiod is over
    if ~dummymode
        Eyelink('Message', 'STIMULUS_DISPLAY');
    end
    tp =0;
    targetFix=0;
    % until maximum trial length check if the the target gets fixated
    while tp <= vblStimOnset + stim.trialLength - 0.5 * screen.ifi
        tp = GetSecs;
        if ~dummymode && Eyelink('NewFloatSampleAvailable') > 0 %
            evt = Eyelink('NewestFloatSample');
            eyeX = evt.gx(el.eye_used+1); % x und y Koordinate
            eyeY = evt.gy(el.eye_used+1);
            if eyeX~=el.MISSING_DATA && eyeY~=el.MISSING_DATA && evt.pa(el.eye_used+1)>0
                distance = sqrt( (tXpos-eyeX)^2 + (tYpos-eyeY)^2 );
                if distance < stim.targetDist
                    targetFix = targetFix+1;
                    if targetFix>20
                        Eyelink('Message', 'GAZE_ON_TARGET');
                        WaitSecs(1);
                        break; % we could add the option to end the trial at
                    end
                end
            end
            
        else % im dummymode machen wir das gleiche mit der Maus
            [mouseX, mouseY, ~] = GetMouse(screen.window);
            distance = sqrt( (tXpos-mouseX)^2 + (tYpos-mouseY)^2 );% distanz zum target
            if distance < stim.targetDist
                targetFix = targetFix+1;
                if targetFix>20
                    disp('GAZE_ON_TARGET')
                    WaitSecs(1);
                    break;
                end
            end
        end
    end
    rt = vblStimOnset - tp;
   
    
    if ~dummymode & ~practice
        WaitSecs(0.1);
        Eyelink('StopRecording');
        % Messages for trial variables.
        % Messages for trial variables.
        Eyelink('Message', '!V TRIAL_VAR participant %s', num2str(ptc.code));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR age %s', num2str(ptc.age));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR gender %s', num2str(ptc.gender));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR handedness %s', num2str(ptc.hand));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR vision %s', num2str(ptc.visionOk));
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR trial %d', t);
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR block %d',b);
        WaitSecs(0.001);
        Eyelink('Message', '!V TRIAL_VAR stim_pos %d', stim.pos);
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_xpos %s', num2str(tXpos));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_ypos %s', num2str(tYpos));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_xpos_deg %s', num2str(tXpos_deg));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR target_ypos_deg %s', num2str(tYpos_deg));
        WaitSecs(0.001)
        Eyelink('Message', '!V TRIAL_VAR reaction_time %s', num2str(rt)); % time the target was presented before the participant response
        WaitSecs(0.001)
        Eyelink('Message', 'TRIAL_RESULT 0');
    end
end