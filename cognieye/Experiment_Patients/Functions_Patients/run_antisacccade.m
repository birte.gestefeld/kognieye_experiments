function run_antisacccade(screen,stim,el,dummymode,ptc,t,b,edfFile, trial)
if dummymode
    ShowCursor
    Screen('ShowCursorHelper', screen.window)
end

% we first select the position of the distractor in
% each trial at random

%bug fix 21.12.
[dXpos, dYpos,dXpos_deg, dYpos_deg] = dotPosition(screen,stim);% this is where the distractor gets displayed
if stim.pos<5
    posD = stim.pos+4;
else
    posD = stim.pos +4 -8;
end
stim.pos=posD;
[tXpos, tYpos, tXpos_deg, tYpos_deg] = dotPosition(screen,stim); % this is the invisible target position


%% then we need to determine the foreperiod, which is also
% randomly selected
fp = (stim.endFP-stim.startFP).* rand + stim.startFP;
% we only present the fixaton cross during the foreperiod

% grey background
Screen('FillRect', screen.window, screen.grey);
Screen('DrawDots', screen.window, [stim.crossXpos, stim.crossYpos], stim.dotSize, screen.black, [], 2); % fixation dot
vblFixOnset=Screen('Flip', screen.window);
fd =0;
fixError = 0;
if ~dummymode
    Eyelink('Message', 'FIXATION_DISPLAY');
end

% now we check for fixation errors (optional)
while fd <= vblFixOnset + fp - 0.5 * screen.ifi
    fd = GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el,dummymode,edfFile,b);
    end
    
end


%% draw distractor
% present the black dot either to the left or to the right
% Screen('DrawLines', screen.window, stim.allCoords, stim.lineWidth, screen.black, [stim.crossXpos, stim.crossyPos], 2);
Screen('DrawDots', screen.window, [dXpos dYpos], stim.dotSize, screen.black, [], 2); % distractor in black, no fixation cross in the center
vblDistOnset=Screen('Flip', screen.window);

if ~dummymode
    Eyelink('Message', 'STIMULUS_DISPLAY');
end
dpt = 0;
% check if participant makes a saccade
while dpt <= vblDistOnset + stim.trialLength - 0.5 * screen.ifi
    dpt=GetSecs;
    [keyIsDown, ~, keyCode, ~] = KbCheck();
    if keyIsDown
        ansKey = find(keyCode);
        check_key(ansKey,el,dummymode,edfFile,b);
    end
    if ~dummymode && Eyelink( 'NewFloatSampleAvailable') > 0 % dazu muessen wir die Daten vom eye tracker abrufen
        evt = Eyelink( 'NewestFloatSample');
        eyeX = evt.gx(el.eye_used+1); % x und y Koordinate
        eyeY = evt.gy(el.eye_used+1);
        if eyeX~=el.MISSING_DATA && eyeY~=el.MISSING_DATA && evt.pa(el.eye_used+1)>0
            distance = sqrt( (stim.crossXpos-eyeX)^2 + (stim.crossYpos-eyeY)^2 ); % from fixation cross
            if distance > stim.fixcrossDist % if the eye position is more than 100 pixels from the fixation cross, we assume that a saccade is in progress
                Eyelink('Message', 'SACCADE');
                WaitSecs(1);
                break;
            end
        end
        
    else % im dummymode machen wir das gleiche mit der Maus
        [mouseX, mouseY, ~] = GetMouse(screen.window);
        distance = sqrt( (stim.crossXpos-mouseX)^2 + (stim.crossYpos-mouseY)^2 ); % distance to the fixation cross
        if distance > stim.fixcrossDist
            disp('SACCADE');
            WaitSecs(1);
            break;
        end
    end
end
rt = dpt-vblDistOnset;

if ~dummymode & ~trial
    WaitSecs(0.1);
    Eyelink('StopRecording');
    % Messages for trial variables.
    Eyelink('Message', '!V TRIAL_VAR participant %s', num2str(ptc.code));
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR age %s', num2str(ptc.age));
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR gender %s', num2str(ptc.gender));
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR handedness %s', num2str(ptc.hand));
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR vision %s', num2str(ptc.visionOk));
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR trial %d', t);
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR block %d',b);
    WaitSecs(0.001);
    Eyelink('Message', '!V TRIAL_VAR stim_pos %d', stim.pos);
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR target_region_xpos %s', num2str(tXpos));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR target_region_ypos %s', num2str(tYpos));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR target_region_xpos_deg %s', num2str(tXpos_deg));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR target_region_ypos_deg %s', num2str(tYpos_deg));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR distractor_xpos %s', num2str(dXpos));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR distractor_ypos %s', num2str(dYpos));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR distractor_xpos_deg %s', num2str(dXpos_deg));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR distractor_ypos_deg %s', num2str(dYpos_deg));
    WaitSecs(0.001)
    Eyelink('Message', '!V TRIAL_VAR reaction_time %s', num2str(rt)); % time the target was presented before the participant response
    WaitSecs(0.001)
    
    % Message of trial end.
    Eyelink('Message', 'TRIAL_RESULT 0');
end


end