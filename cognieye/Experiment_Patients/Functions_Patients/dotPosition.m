
function [dotXpos, dotYpos, degXpos, degYpos] = dotPosition(screen,stim)
%   determines the position of the dot on the screen in pixels and degrees for 8 different positions.
%       INPUT:
%           screen = struct with screen info
%           stim = struct with stimulus info
%       OUTPUT:
%           dotXpos = position of the dot in pixels horizontally
%           dotYpos = position of the dot in pixels vertically
%           degXpos = position of the dot in degrees of vis. angle horizontally
%           degYpos = position of the dot in degrees of vis. angle vertically
% Birte Gestefeld, 2021, birte.gestefeld@uni-bielefeld.de
% Christian Poth, 2021, c.poth@uni-bielefeld.de
% 
    switch stim.pos
        case 1 % nach rechts
            degXpos = round(stim.sacLen*cos(0));
            degYpos = round(stim.sacLen*sin(0));
        case 2
            degXpos = round(stim.sacLen*cos(pi/4));
            degYpos = round(stim.sacLen*sin(pi/4));
        case 3
            degXpos = round(stim.sacLen*cos(pi/2));
            degYpos = round(stim.sacLen*sin(pi/2));
        case 4
            degXpos = round(stim.sacLen*cos(pi*3/4));
            degYpos = round(stim.sacLen*sin(pi*3/4));
        case 5
            degXpos = round(stim.sacLen*cos(pi));
            degYpos = round(stim.sacLen*sin(pi));
        case 6
            degXpos = round(stim.sacLen*cos((2*pi/8)*5));
            degYpos = round(stim.sacLen*sin((2*pi/8)*5));
        case 7
            degXpos = round(stim.sacLen*cos((2*pi/8)*6));
            degYpos = round(stim.sacLen*sin((2*pi/8)*6));
        case 8
            degXpos = round(stim.sacLen*cos((2*pi/8)*7));
            degYpos = round(stim.sacLen*sin((2*pi/8)*7));
    end
    
    % Umwandlung in pixels
    pixXpos=deg2pix(degXpos, screen.xScreen, screen.width, screen.viewDist);
    pixYpos=deg2pix(degYpos, screen.yScreen, screen.height, screen.viewDist)*-1;
    % ins zentrum des Screens
    dotXpos = pixXpos + screen.xCenter;
    dotYpos = pixYpos + screen.yCenter;
end

